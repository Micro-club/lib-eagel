# Lib Eagel
kumpulan library eagel

## Cara Instal ke Eagel
1. Download librari->ekstak kemudian copy
2. Buka folder dimana eagel diinstal -> cari folder lbr
3. Pastekan file yang sebelumnya dicopy

### Komponen Sering dipakai 
1. TUXGR_16X2_R2 -> LCD16x2
2. R-EU* -> All Resistor
3. C-EU* -> All Cap Non-Polar
4. CPOL-EU* -> All Cap Polar
5. 7805* -> Reguator 5 volt
6. 10-xx -> Switch 4 Pin
7. G5L -> Relay 5 Pin
8. 351 -> Relay 8 Pin
9. led3mm* -> LED 3 mm
10. led5mm* -> led 5 mm
11. irf9530 -> Mosfet irf 9530 P - Channel
12. irf530 -> Mosfet irf 530  N - Channel
13. bd139* -> NPN transistor BD139
14. bd140* -> PNP transistor BD140
15. 2sc945 -> NPN transistor driver relay 
16. 74595* -> 8-bit shift register IC 74595
17. f/qmx -> Buzzer
18. 1n4004 -> diode rectifier